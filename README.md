# Minimal test project for the Spring Security SwitchUserFilter

This project is meant to serve as a minimal project to test the SwitchUserFilter. 

The main branch uses Spring Security 5.8 and can be used as follows:
* Start the Spring application with `./gradlew bootRun`
* Open `http://localhost:8080` in a web browser
* Use `admin` (both username and password) to login
* The headline should read "Logged in as admin"
* Click the link to navigate to the `/admin` page
* Click on the button to switch to the non-admin user
* The headline should now read "Logged in as user"
* Click the button to switch back to the admin user

This however does not work in the [Spring Security 6 branch](https://gitlab.com/robertbleyl/switch-user-test/-/tree/spring_security_6). The switch to the non-admin user does not work. You are redirected to the index page, but you remain logged in as the admin user.

The log level for Spring Security is set to `DEBUG` and shows a line containing `Failed to find original user` which might help in finding the issue.
