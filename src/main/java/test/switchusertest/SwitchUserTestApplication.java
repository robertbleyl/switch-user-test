package test.switchusertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwitchUserTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwitchUserTestApplication.class, args);
    }
}
