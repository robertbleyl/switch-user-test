package test.switchusertest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/")
    public String getIndexPage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        boolean isImpersonating = authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_PREVIOUS_ADMINISTRATOR"));

        model.addAttribute("username", username);
        model.addAttribute("displayAdminPageLink", username.equals("admin"));
        model.addAttribute("displayExitSwitchUserLink", isImpersonating);
        return "index";
    }

    @GetMapping("/admin")
    public String getAdminPage() {
        return "admin";
    }
}
